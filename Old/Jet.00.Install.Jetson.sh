#!/bin/bash

## execute bash Install.Jetson.sh

GREEN='\033[0;32m'
NC='\033[0m'

#remove extras Jetson
echo "Removendo extras Jetson"
sudo apt-get -y remove --purge libreoffice*
clear
echo Removendo extras Jetson
sudo apt-get -y purge thunderbird*
sudo apt-get -y purge --auto-remove gnome-calendar
sudo apt-get -y purge --auto-remove rhythmbox
sudo apt -y purge aisleriot gnome-mahjongg gnome-mines gnome-sudoku
clear
echo Removendo extras Jetson
sudo apt-get -y remove vim
sudo apt-get -y install nano
sudo apt-get -y install htop
sudo apt clean
sudo apt-get -y autoremove

clear

echo "Update Linux"
sudo apt-get update
clear

#Instalar o openALPR
echo "instalando OpenAlpr"


clear
echo "Testando OpenAlpr"
sudo wget http://plates.openalpr.com/ea7the.jpg
alpr -c us ea7the.jpg


clear
#install Python
echo "Instalando recursos python"

python -m pip install --upgrade pip
python3 -m pip install --upgrade pip

pip install --upgrade pip

pip3 install openalpr
pip3 install numpy
#pip3 install opencv-python não funciona para ARM


clear
echo "Verifcando versão do Python CV2, recomendado 4.2.0 ou superior"
python3 -c "import cv2; print(cv2.__version__)"


